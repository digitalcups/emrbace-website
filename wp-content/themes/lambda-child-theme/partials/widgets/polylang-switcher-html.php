<div class="item">
    <ul class="languages">
		<?php
		$__current__url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
		$homepage_url = site_url() . "/" . pll_current_language() . "/" ;
		$homepage_url = preg_replace("(^https?://)", "", $homepage_url ); // remove http:/ or https:/ from the url
		if ($__current__url == $homepage_url) {
			pll_the_languages(array('display_names_as' => 'slug'));
		} else {
			$languages = pll_the_languages(array("raw" => 1, 'hide_if_empty' => 0, 'display_names_as' => 'slug'));
			foreach ($languages as $lang) {
				if ($lang['slug'] == pll_current_language()) {
					echo '<li class="current-lang"><a class="hover-blue" hreflang="' . $lang['locale'] . '" href="' . $lang['url'] . '">' . $lang['name'] . '</a></li>';
				} else {
					$other_lang = (pll_current_language() == "en" ? "ar" : "en");
					if ($lang['url'] == site_url() . "/" . $other_lang . "/") {
						echo '<li><a class="hover-blue" style="text-decoration: line-through !important;">' . $lang['name'] . '</a></li>';
					} else {
						echo '<li><a hreflang="' . $lang['locale'] . '" href="' . $lang['url'] . '">' . $lang['name'] . '</a></li>';
					}
				}
			}
		}
		?>
    </ul>
</div>