<?php
// Creating the widget
class dp_polylang_switcher extends WP_Widget {

	function __construct() {
		parent::__construct(

// Base ID of your widget
			'dp_polylang_switcher',

// Widget name will appear in UI
			__('DP Polylang Switcher', 'dp_polylang_switcher'),

// Widget description
			array( 'description' => __( 'Polylang Switcher using slugs', 'dp_polylang_switcher' ), )
		);
	}

// Creating widget front-end

	public function widget( $args, $instance ) {
		echo '<div class="language-switcher"><ul class="languages">';
		$languages = pll_the_languages(array("raw" => 1, 'hide_if_empty' => 0, 'display_names_as' => 'slug'));
		foreach ($languages as $lang) {
			if ($lang['slug'] != pll_current_language()) {
				echo '<li><a hreflang="' . $lang['locale'] . '" href="' . $lang['url'] . '">' . $lang['name'] . '</a></li>';
			}
		}
    echo '</ul></div>';
	}

} // Class wpb_widget ends here