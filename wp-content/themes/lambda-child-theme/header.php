<?php
/**
 * Displays the head section of the theme
 *
 * @package Lambda
 * @subpackage Frontend
 * @since 0.1
 *
 * @copyright (c) 2015 Oxygenna.com
 * @license http://wiki.envato.com/support/legal-terms/licensing-terms/
 * @version 1.53.2
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" /><?php

        $site_icon_id = get_option('site_icon');
        if ($site_icon_id === '0' || $site_icon_id === false) {
            oxy_favicons();
        }

        wp_head(); ?>

        <?php
        if(pll_current_language() == 'ar') { ?>
<!--             <link href="https://fonts.googleapis.com/css?family=Cairo:200,300,400,600,700,900&amp;subset=arabic" rel="stylesheet"> -->
<!-- 			<link href="https://fonts.googleapis.com/css?family=Lateef&amp;subset=arabic" rel="stylesheet"> -->
			<link href="https://fonts.googleapis.com/css?family=El+Messiri:400,500,600,700&amp;subset=arabic" rel="stylesheet">
        <?php } ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119709803-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-119709803-1');
        </script>

    </head>
    <body <?php body_class(); ?>>
        <div class="pace-overlay"></div>
        <?php oxy_create_nav_header(); ?>
        <div id="content" role="main">